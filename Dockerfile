FROM java:8-jdk
MAINTAINER Alex Proca <alex.proca@gmail.com>

RUN curl -O https://downloads.typesafe.com/play/2.2.3/play-2.2.3.zip
RUN unzip play-2.2.3.zip
RUN mv play-2.2.3 play
RUN ln -s /play/play /usr/bin/play

